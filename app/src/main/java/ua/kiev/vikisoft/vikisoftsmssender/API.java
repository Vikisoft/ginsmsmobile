package ua.kiev.vikisoft.vikisoftsmssender;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.provider.Settings;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;

public class API {

    public static final String SERVER_ROOT = "/toothfairy/services/GinMobileJson.php";

    public static final String KEY_SMS = "sms";

    public static final String SP_KEY_LICENSE        = "license";
    public static final String SP_KEY_SMS_START_TIME = "SMSStartTime";
    public static final String SP_KEY_SMS_END_TIME   = "SMSEndTime";
    public static final String SP_KEY_SEND_DAYS      = "SMSSendDays";

    public static final int API_CHECK_ID = 0;
    public static final int API_GET_SMS_ID = 1;
    public static final int API_GET_SMS_STATUS_ID = 2;
    public static final int API_CLEAR_DB_ID = 3;

    static long testSMSId = 100000000;

//    API METHODS

//    for first connection
    public static String funcCheck(String serverUrl){

        String param1 = "func=check";
        String param2 = "lic" + "=" + getDecodedLicense();

        String urlStr = "http://" + serverUrl + SERVER_ROOT +
                "?" +
                param1 +
                "&" +
                param2;

        return urlStr;
    }

    public static String funcCheck(){

        String param1 = "func=check";
        String param2 = "lic" + "=" + getDecodedLicense();

        String urlStr = getServerUrl() +
                "?" +
                param1 +
                "&" +
                param2;

        return urlStr;
    }
    public static String funcGetSms(){

        String param1 = "func=getSMS";
        String param2 = "lic" + "=" + getDecodedLicense();

        String urlStr = getServerUrl() +
                "?" +
                param1 +
                "&" +
                param2;

        return urlStr;
    }
    public static String funcSetSmsStatus(String smsid, String status){

        String param1 = "func=setSMSStatus";
        String param2 = "smsid=" + smsid;
        String param3 = "status=" + status;
        String param4 = "lic" + "=" + getDecodedLicense();

        String urlStr = getServerUrl() +
                "?" +
                param1 +
                "&" +
                param2 +
                "&" +
                param3 +
                "&" +
                param4;

        return urlStr;
    }
    public static String funcClearDB(){

        String param1 = "func=devFunc_clearDB";
//        String param2 = "lic" + "=" + getDecodedLicense();

        String urlStr = getServerUrl() +
                "?" +
                param1/* +
                "&" +
                param2*/;

        return urlStr;
    }

//    Test methods


    public static String funcCheckTESTRESPONSE() {

        String KEY_LICENSE = "license";

        String KEY_SETTINGS = "settings";
        String KEY_SETTINGS_SMS_START_TIME = "SMSStartTime";
        String KEY_SETTINGS_SMS_END_TIME = "SMSEndTime";
        String KEY_SETTINGS_SMS_SEND_DAYS = "SMSSendDays";

        JSONObject json = new JSONObject();
        try {
            String license = ((int)(1000* Math.random()) % 2 == 0) ? "2925253125" : "GIN_SMS_LIC_ERROR";
            json.put(KEY_LICENSE, license);


            JSONObject settings = new JSONObject();

            settings.put(KEY_SETTINGS_SMS_START_TIME, "9:00");
            settings.put(KEY_SETTINGS_SMS_END_TIME, "20:00");
            settings.put(KEY_SETTINGS_SMS_SEND_DAYS, "1111100");

            json.put(KEY_SETTINGS, settings);
        }
        catch (JSONException e) {
            // TODO Auto-generated catch block
        }

        return json.toString();
    }

    public static String funcGetSmsTESTRESPONSE() {

        String KEY_ERRROR = "error";

        String KEY_SETTINGS = "settings";
        String KEY_SETTINGS_SMS_START_TIME = "SMSStartTime";
        String KEY_SETTINGS_SMS_END_TIME = "SMSEndTime";
        String KEY_SETTINGS_SMS_SEND_DAYS = "SMSSendDays";


        JSONObject json = new JSONObject();
        try {
//          >> JSON : ERROR
            json.put(KEY_ERRROR, "");

//          >> JSON : SMS
            JSONArray smsArr = new JSONArray();

            int randSmsCount = (int) (5*Math.random()+1);

            for(int i = 0; i < randSmsCount; i++){

                JSONObject sms = new JSONObject();

                sms.put(SMS.KEY_SMS_ID, "" + (testSMSId++));
                sms.put(SMS.KEY_PHONE, "+380986103129");
                sms.put(SMS.KEY_RESPONSE, "100");
                sms.put(SMS.KEY_MESSAGE, randomMessage());
                sms.put(SMS.KEY_LIVETIME, "1457815873");

                smsArr.put(sms);
            }

            json.put(KEY_SMS, smsArr);

//          >> JSON : SETTINGS
            JSONObject settings = new JSONObject();

            settings.put(KEY_SETTINGS_SMS_START_TIME, "9:00");
            settings.put(KEY_SETTINGS_SMS_END_TIME, "20:00");
            settings.put(KEY_SETTINGS_SMS_SEND_DAYS, "1111111");

            json.put(KEY_SETTINGS, settings);
        }
        catch (JSONException e) {
            // TODO Auto-generated catch block
        }

        return json.toString();
    }

    private static String getServerUrl() {

        Context context = null;
        if(MainActivity.activity != null)
            context = MainActivity.activity;
        else if(ProcessSMSService.service != null)
            context = ProcessSMSService.service;
        else{
            return "";
        }

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return "http://" + prefs.getString(Constants.KEY_SERVER_URL, "") + SERVER_ROOT;
    }

    private static String randomMessage() {

        int messLengthRand = (int) (Math.random()*100)+50;

        StringBuilder sbMess = new StringBuilder();

        for(int i = 0; i < messLengthRand; i++){
            int randCharCode = (int) ((Math.random()*('z' - 'a')) + 'a');
            if(randCharCode%7 != 0)
                sbMess.append((char)randCharCode);
            else
                sbMess.append(' ');
        }

        return sbMess.toString();
    }

//    Service methods

    public static String getDecodedLicense() {

        Context context = null;
        if(MainActivity.activity != null)
            context = MainActivity.activity;
        else if(ProcessSMSService.service != null)
            context = ProcessSMSService.service;
        else{
            return "";
        }

        String android_id = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);

//        TEST
//        if( !android_id.equals(Constants.TEST_DEVICE_ID))
//            android_id = Constants.TEST_DEVICE_ID;

        return API.decodeLicense(android_id.toUpperCase().substring(0,5));
    }

    public static String decodeLicense(String deviceID){

//        deviceID = "IJK";

        String retString  = "";
        int i = 0;
        String hexValue = null;

        String utf = deviceID;
        byte[] bytesASCII = null;
        try {
            bytesASCII = utf.getBytes("ASCII");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

//        for (int k=0; k < licenseRawRequest.length(); k++)
        for (int k=0; k < bytesASCII.length; k++)
        {
//            i = s.charCodeAt(k);//переводим каюдый символ строки в ASCII
            i = bytesASCII[k];
//            hexValue = i.toString(16);//каждый символ переводим в HEX
            hexValue = Integer.toHexString(i);

            int hexValueAsDecDigit = Integer.parseInt(hexValue);

            switch (hexValue.length())
            {
                case 2:
//                    hexValue = String(Math.round((int(hexValue)+1)*89/120));
                    hexValue = "" + Math.round( ( ( hexValueAsDecDigit + 1 ) * 89.0 / 120.0 ) );
                    break;
                case 1:
//                    hexValue = String(Math.round((int(hexValue)+1)*33/38));
                    hexValue = "" + Math.round( ( ( hexValueAsDecDigit + 1 ) * 33.0 / 38.0 ) );
                    break;
            }

            retString += hexValue;
        }
        return retString;


    }

    public static String dayToday() {  // monday=1, sunday=7

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);

// If current day is Sunday, day=1. Saturday, day=7.

        switch (day) {
            case Calendar.MONDAY:
                return Constants.MONDAY;
            case Calendar.TUESDAY:
                return Constants.TUESDAY;
            case Calendar.WEDNESDAY:
                return Constants.WEDNESDAY;
            case Calendar.THURSDAY:
                return Constants.THURSDAY;
            case Calendar.FRIDAY:
                return Constants.FRIDAY;
            case Calendar.SATURDAY:
                return Constants.SATURDAY;
            case Calendar.SUNDAY:
                return Constants.SUNDAY;
            default:
                return null;
        }
    }

    public static String getDayNameById(int id) {



        return "";
    }

    public static String makeSendDaysJson(String smsSendDays) {

        char[] daysSend = smsSendDays.toCharArray();

        JSONObject json = new JSONObject();
        try {
            json.put(Constants.MONDAY,    daysSend[0]=='1');
            json.put(Constants.TUESDAY,   daysSend[1]=='1');
            json.put(Constants.WEDNESDAY, daysSend[2]=='1');
            json.put(Constants.THURSDAY,  daysSend[3]=='1');
            json.put(Constants.FRIDAY,    daysSend[4]=='1');
            json.put(Constants.SATURDAY,  daysSend[5]=='1');
            json.put(Constants.SUNDAY,    daysSend[6]=='1');

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return json.toString();
    }
}
