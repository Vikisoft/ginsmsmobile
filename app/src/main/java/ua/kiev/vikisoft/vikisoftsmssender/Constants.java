package ua.kiev.vikisoft.vikisoftsmssender;

/**
 * Created by Pavel on 10.03.2016.
 */
public class Constants {

    public static final int GET_SMS_PERIOD        = 1000 * 60 * 15;

    public static final int GET_SMS_PERIOD_STEP   = 1000 * 3;

    public static final int PROCESS_ONE_SMS_TIME  = 10000;

    public static final int LAST_SAVED_SMS_AMOUNT = 100;

    public static final int SMS_LIVE_TIME         = 1000 * 60 * 60;

    public static final String KEY_SERVER_URL = "KEY_SERVER_URL";
    public static final String KEY_SMS_JSON   = "KEY_SMS_JSON";

    public static final String KEY_IS_SERVER_RUNNING = "KEY_IS_SERVER_RUNNING";

    public static final String KEY_INTENT_PHONE  = "KEY_INTENT_PHONE";
    public static final String KEY_INTENT_SMS_ID = "KEY_INTENT_SMS_ID";

    public static final String SUNDAY    = "SUNDAY";
    public static final String MONDAY    = "MONDAY";
    public static final String TUESDAY   = "TUESDAY";
    public static final String WEDNESDAY = "WEDNESDAY";
    public static final String THURSDAY  = "THURSDAY";
    public static final String FRIDAY    = "FRIDAY";
    public static final String SATURDAY  = "SATURDAY";

    public static final String TEST_DEVICE_ID = "43627";
//    43627 - MY HTC
}
