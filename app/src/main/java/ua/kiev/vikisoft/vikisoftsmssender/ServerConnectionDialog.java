package ua.kiev.vikisoft.vikisoftsmssender;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

//import org.apache.http.HttpResponse;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.impl.client.DefaultHttpClient;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ServerConnectionDialog extends DialogFragment implements OnClickListener {

  Context context;

  TextView tvDeviceId;
  EditText etServerIp;

  final String LOG_TAG = "myLogs";

  public void onClick(DialogInterface dialog, int which) {
    int i = 0;
    switch (which) {
    case Dialog.BUTTON_POSITIVE:
//      i = R.string.yes;
      break;
    case Dialog.BUTTON_NEGATIVE:
//      i = R.string.no;
      break;
    case Dialog.BUTTON_NEUTRAL:
//      i = R.string.maybe;
      break;
    }
    if (i > 0)
      Log.d(LOG_TAG, "Dialog 2: " + getResources().getString(i));
  }

  @Override
  public void onAttach(Activity activity) {
    super.onAttach(activity);
    context = activity.getApplicationContext();
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    this.context = context;
  }

  public void onDismiss(DialogInterface dialog) {
    super.onDismiss(dialog);
    Log.d(LOG_TAG, "Dialog 2: onDismiss");
  }

  public void onCancel(DialogInterface dialog) {
    super.onCancel(dialog);
    Log.d(LOG_TAG, "Dialog 2: onCancel");
  }

  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {

    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

    // Get the layout inflater
    LayoutInflater inflater = getActivity().getLayoutInflater();

    View rootView = inflater.inflate(R.layout.server_connect_dialog_layout, null, false);

    tvDeviceId = (TextView) rootView.findViewById(R.id.tvDeviceIdValue);
    String android_id = Settings.Secure.getString(getActivity().getContentResolver(),
            Settings.Secure.ANDROID_ID);
    tvDeviceId.setText(android_id.toUpperCase().substring(0,5));

    etServerIp = (EditText) rootView.findViewById(R.id.etServerIp);

    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
    if(prefs.contains(Constants.KEY_SERVER_URL)) {
      etServerIp.setText(prefs.getString(Constants.KEY_SERVER_URL, ""));
    }



    builder.setTitle(R.string.connect_to_server);

    builder.setCancelable(false);

    // Inflate and set the layout for the dialog
    // Pass null as the parent view because its going in the dialog layout
    builder.setView(rootView)
            // Add action buttons
            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int id) {
                // sign in the user ...

                String serverUrl = etServerIp.getText().toString();

                if (!TextUtils.isEmpty(serverUrl)) {

                  new ServerConnectionThread(getActivity(),
                                             etServerIp.getText().toString(),
                                             tvDeviceId.getText().toString()
                                            ).start();
                }


              }
            })
            .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int id) {
                Dialog thisDialog = ServerConnectionDialog.this.getDialog();
                if (thisDialog != null)
                  thisDialog.dismiss();
              }
            });


//    >> TestData:
    tvDeviceId.setText(Constants.TEST_DEVICE_ID);
    etServerIp.setText("server2.vikisoft.com.ua/toothfairyhost/smsdev");

    return builder.create();
  }

  // convert inputstream to String
  private static String convertInputStreamToString(InputStream inputStream) throws IOException {
    BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
    String line = "";
    String result = "";
    while((line = bufferedReader.readLine()) != null)
      result += line;

    inputStream.close();
    return result;

  }

  class ServerConnectionThread extends Thread{

    Context context;
    String license;
    String ip;

    public ServerConnectionThread(Context context, String ip, String license) {
      this.context = context;
      this.ip = ip;
      this.license = license;
    }

    @Override
    public void run() {

      String response = null;

      HttpURLConnection urlConnection = null;
      try {

        String urlStr = API.funcCheck(ip);

        URL url = new URL(urlStr);

        urlConnection = (HttpURLConnection) url.openConnection();

        InputStream in = urlConnection.getInputStream();

        InputStreamReader isw = new InputStreamReader(in);

        StringBuilder sb = new StringBuilder();

        char[] buffer = null;
        while (isw.read(buffer = new char[50]) != -1) {
          sb.append(new String(buffer));
        }

        response = sb.toString();

        JSONObject jsonRoot = new JSONObject(response);

//               >> JSON : ERROR
        final String errorStr = jsonRoot.optString("error");
        if(errorStr.length() > 0){

          FragmentManager fm = MainActivity.activity.getFragmentManager();
          ConnectionResultDialog sdf = new ConnectionResultDialog();
          Bundle b = new Bundle();
          b.putBoolean(ConnectionResultDialog.KEY_CONNECTION, false);
          sdf.setArguments(b);
          sdf.show(fm, "dialog");

        }
        else{
          SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
          SharedPreferences.Editor editor = prefs.edit();
          editor.putString(Constants.KEY_SERVER_URL, ip);
          editor.commit();

          FragmentManager fm = MainActivity.activity.getFragmentManager();
          ConnectionResultDialog sdf = new ConnectionResultDialog();
          Bundle b = new Bundle();
          b.putBoolean(ConnectionResultDialog.KEY_CONNECTION, true);
          sdf.setArguments(b);
          sdf.show(fm, "dialog");
        }
      } catch (Exception e) {

        FragmentManager fm = MainActivity.activity.getFragmentManager();
        ConnectionResultDialog sdf = new ConnectionResultDialog();
        Bundle b = new Bundle();
        b.putBoolean(ConnectionResultDialog.KEY_CONNECTION, false);
        sdf.setArguments(b);
        sdf.show(fm, "dialog");

      } finally {
        if (urlConnection != null) {
          urlConnection.disconnect();
        }
      }
    }
  }

  public static class ConnectionResultDialog extends DialogFragment {

    public static String KEY_CONNECTION = "KEY_CONNECTION";

    final String LOG_TAG = "myLogs";

    public Dialog onCreateDialog(Bundle savedInstanceState) {

      Bundle b = getArguments();
      boolean isConnection = b.getBoolean(KEY_CONNECTION);

      AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
              .setTitle(isConnection ? R.string.connection_success : R.string.connection_error)
              .setMessage(isConnection ? R.string.congratulations : R.string.check_credentials)
              .setPositiveButton(android.R.string.ok, null);
      return adb.create();
    }
  }

}