package ua.kiev.vikisoft.vikisoftsmssender;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.content.Intent;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static ua.kiev.vikisoft.vikisoftsmssender.Constants.LAST_SAVED_SMS_AMOUNT;
import static ua.kiev.vikisoft.vikisoftsmssender.Constants.SMS_LIVE_TIME;

public class ProcessSMSService extends Service {

    public static Service service;

    public static boolean isServiceRun;

    Handler h;
    MainBackgroundThread t;

    private BroadcastReceiver smsSendReceiver;
    private BroadcastReceiver smsDeliveredReceiver;

    @Override
    public IBinder onBind(Intent arg0) {
        log("ON BIND!!!!", true, true);
        return null;
    }

    public void onRebind(Intent intent) {
        super.onRebind(intent);
        log("ON RE_BIND!!!!", true, true);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        log("ON UN_BIND!!!!", true, true);
        return super.onUnbind(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        log("Service Started", true, true);

        service = this;

        smsSendReceiver = new SMSSendReceiver();
        smsDeliveredReceiver = new SMSDeliveredReceiver();

// For when the SMS has been sent
        registerReceiver(smsSendReceiver, new IntentFilter(SMSSendReceiver.ACTION_SMS_SENT));

// For when the SMS has been delivered
        registerReceiver(smsDeliveredReceiver, new IntentFilter(SMSDeliveredReceiver.ACTION_SMS_DELIVERED));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        log("Service onStartCommand", true, true);

//        Let it continue running until it is stopped.
        isServiceRun = true;

        h = new Handler(this.getMainLooper());


        if (t != null && !t.isAlive()) {
            t = null;
        }


        if (t == null)
            t = new MainBackgroundThread(this);
        if (!t.isAlive())
            t.start();

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

//        log("Service DESTROYED", true, true);

        unregisterReceiver(smsSendReceiver);
        unregisterReceiver(smsDeliveredReceiver);

//        log("RECEIVERS _ UNREGISTERED", true, true);

        t.stopMe();

        isServiceRun = false;
    }

    class MainBackgroundThread extends Thread {

        volatile boolean finished = false;

        Context context;
        String response = null;

        public MainBackgroundThread(Context context) {
            this.context = context;
        }

        public void stopMe() {
            finished = true;
        }

        @Override
        public void run() {

            do {

                isServiceRun = true;

//            clearDB_TEMP_TEST();
                response = getSmsFromServer();

//            TEST RESPONSE
//            response = API.funcGetSmsTESTRESPONSE();
//
//            vibrate();


                if (TextUtils.isEmpty(response)){
//                    log("response is EMPTY!!", true, true);
                    sleepGetSMSPeriod();
                    continue;
                }
                else{
//                    log(response, true, true);
                }


                try {
                    JSONObject jsonRoot = new JSONObject(response);


//               >> JSON : ERROR
                    final String errorStr = jsonRoot.optString("error");
                    if (errorStr.length() != 0) {
//                        log("Error from Server : " + errorStr, true, true);
                        sleepGetSMSPeriod();
                        continue;
                    }


//               >> JSON : SETTINGS
                    JSONObject jsonSettings = jsonRoot.optJSONObject("settings");
                    boolean letsSendSMS = processSettings(jsonSettings);
//                TEST
//               boolean letsSendSMS = true;

                    if (!letsSendSMS) {
                        log("Sending SMS Forbideden!! by System Settings : " + errorStr, true, true);
                        sleepGetSMSPeriod();
                        continue;
                    }


//               >> JSON : SMS
                    JSONArray jsonSMSArr = jsonRoot.optJSONArray("sms");
                    processSMS(jsonSMSArr);


                } catch (final JSONException e) {
                    log("General Error : " + e.getMessage(), true, true);
                }

                sleepGetSMSPeriod();

            }
            while (!finished);

        }

        private void sleepGetSMSPeriod() {

            response = "";

            long timeIntervals = 0;
            while (timeIntervals < Constants.GET_SMS_PERIOD) {

                if (finished)
                    break;

                try {
                    Thread.sleep(Constants.GET_SMS_PERIOD_STEP);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                timeIntervals += Constants.GET_SMS_PERIOD_STEP;

            }
        }

        private boolean processSettings(JSONObject jsonSettings) throws JSONException {

            String SMSStartTimeStr = jsonSettings.optString("SMSStartTime");
            String SMSEndTimeStr = jsonSettings.optString("SMSEndTime");
            String SMSSendDays = jsonSettings.optString("SMSSendDays");

            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(ProcessSMSService.this);
            SharedPreferences.Editor editor = sp.edit();
            editor.putString(API.SP_KEY_SMS_START_TIME, SMSStartTimeStr);
            editor.putString(API.SP_KEY_SMS_END_TIME, SMSEndTimeStr);
            editor.putString(API.SP_KEY_SEND_DAYS, API.makeSendDaysJson(SMSSendDays));
            editor.commit();


            int startHour = Integer.parseInt(SMSStartTimeStr.split(":")[0]);
            int startMinutes = Integer.parseInt(SMSStartTimeStr.split(":")[1]);

            Calendar startCalendar = Calendar.getInstance();
            startCalendar.set(Calendar.HOUR_OF_DAY, startHour);
            startCalendar.set(Calendar.MINUTE, startMinutes);


            int endHour = Integer.parseInt(SMSEndTimeStr.split(":")[0]);
            int endMinutes = Integer.parseInt(SMSEndTimeStr.split(":")[1]);

            Calendar endCalendar = Calendar.getInstance();
            endCalendar.set(Calendar.HOUR_OF_DAY, endHour);
            endCalendar.set(Calendar.MINUTE, endMinutes);


            Calendar calNow = Calendar.getInstance();
            calNow.setTime(new Date());


            String weekScheduleStr = sp.getString(API.SP_KEY_SEND_DAYS, "");
            JSONObject weekScheduleJson = new JSONObject(weekScheduleStr);

            String dayToday = API.dayToday();

//               >>>>   if no need to send sms today
            if (!weekScheduleJson.optBoolean(dayToday))
                return false;

            Date timeNow = calNow.getTime();
            Date timeS = startCalendar.getTime();
            Date timeE = endCalendar.getTime();

//               >>>>   if time not in range [9:00 - 20:00]  - pass sending
            if (calNow.before(startCalendar) || calNow.after(endCalendar))
                return false;


            return true;
        }

        private void processSMS(JSONArray jsonSMSNew) throws JSONException {

//       Add new sms array to existings into Preferenceses

            JSONArray jsonSMSArrAll = new JSONArray();
            JSONArray needToSend = new JSONArray();

//         Add new sms got just from server
            for (int i = 0; i < jsonSMSNew.length(); i++) {
                JSONObject smsNewJson = jsonSMSNew.optJSONObject(i);

                smsNewJson.put(SMS.KEY_IS_NEW, true);

                Date today = new Date();
                DateFormat df = new SimpleDateFormat("dd.MM.yyyy  HH:mm:ss");
                String reportDate = df.format(today);
                smsNewJson.put(SMS.KEY_SHOW_DATE, reportDate);

                jsonSMSArrAll.put(smsNewJson);
                needToSend.put(smsNewJson);
            }

//         Add existed sms saved in SharedPrefs
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            if (prefs.contains(Constants.KEY_SMS_JSON)) {

                JSONArray smsSavedJsonArr = new JSONArray(prefs.getString(Constants.KEY_SMS_JSON, ""));
                for (int i = 0; i < smsSavedJsonArr.length(); i++) {

//               rewrite less than LAST_SAVED_SMS_AMOUNT(=100) last sms
                    if (jsonSMSArrAll.length() >= LAST_SAVED_SMS_AMOUNT)
                        break;

                    JSONObject smsSavedJson = smsSavedJsonArr.optJSONObject(i);
                    smsSavedJson.put(SMS.KEY_IS_NEW, false);
                    jsonSMSArrAll.put(smsSavedJson);

                    int response = smsSavedJson.optInt(SMS.KEY_RESPONSE);
                    if ((response == SMS.SMS_STATUS_WAIT || response == SMS.SMS_STATUS_NOT_SEND) && response > 99)
                        needToSend.put(smsSavedJson);
                }
            }

            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(Constants.KEY_SMS_JSON, jsonSMSArrAll.toString());
            editor.commit();

            h.post(new Runnable() {
                @Override
                public void run() {
                    if (MainActivity.activity != null)
                        MainActivity.activity.smsStatusChanged();
                }
            });


//       Process new sms array - send and wait for delivery response

            for (int i = 0; i < needToSend.length(); i++) {

                if (finished) {
                    break;
                }

                JSONObject sms = needToSend.optJSONObject(i);

                final String smsSmsId = sms.optString(SMS.KEY_SMS_ID);
                String smsPhone = "+" + sms.optString(SMS.KEY_PHONE);    // Problem in v1.0.0 - there were no + sign !!
                String smsResponse = sms.optString(SMS.KEY_RESPONSE);
                String smsMessage = sms.optString(SMS.KEY_MESSAGE);
                long smsLivetime = sms.optLong(SMS.KEY_LIVETIME);

//            Send sms if time of sending (now) is earlier then 1 hour to visit patient time
                if (Math.abs((System.currentTimeMillis() - smsLivetime)) >= SMS_LIVE_TIME) {

                    sendSMS(context, h, smsSmsId, smsPhone, smsMessage);

                    try {
                        Thread.sleep(Constants.PROCESS_ONE_SMS_TIME);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        }

        private void clearDB_TEMP_TEST() {
            HttpURLConnection urlConnection = null;
            try {
                String urlStr = API.funcClearDB();
                URL url = new URL(urlStr);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
        }

        private String getSmsFromServer() {

            String response = null;

            HttpURLConnection urlConnection = null;
            try {

                String urlStr = API.funcGetSms();

                URL url = new URL(urlStr);

                urlConnection = (HttpURLConnection) url.openConnection();

                InputStream in = urlConnection.getInputStream();

                InputStreamReader isr = new InputStreamReader(in);

                StringBuilder sb = new StringBuilder();

                char[] buffer = null;
                while (isr.read(buffer = new char[50]) != -1) {
                    sb.append(new String(buffer));
                }

                response = sb.toString();
            } catch (Exception e) {
                response = "";
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }

            return response;
        }
    }

    class SMSSendThread extends Thread {

        Context context;
        Handler h;
        String smsId;
        String smsPhone;
        String smsMessage;

        public SMSSendThread(Context context, Handler h, String smsId, String smsPhone, String smsMessage) {
            this.context = context;
            this.h = h;
            this.smsId = smsId;
            this.smsPhone = smsPhone;
            this.smsMessage = smsMessage;
        }

        @Override
        public void run() {

            // Get the default instance of SmsManager
            SmsManager smsManager = SmsManager.getDefault();

            Intent smsSendIntent = new Intent(SMSSendReceiver.ACTION_SMS_SENT);
            Intent smsDeliveredIntent = new Intent(SMSDeliveredReceiver.ACTION_SMS_DELIVERED);

            smsSendIntent.putExtra(Constants.KEY_INTENT_PHONE, smsPhone);
            smsSendIntent.putExtra(Constants.KEY_INTENT_SMS_ID, smsId);

            smsDeliveredIntent.putExtra(Constants.KEY_INTENT_PHONE, smsPhone);
            smsDeliveredIntent.putExtra(Constants.KEY_INTENT_SMS_ID, smsId);

            PendingIntent sentPendingIntent = PendingIntent.getBroadcast(context, 0, smsSendIntent, PendingIntent.FLAG_ONE_SHOT);
            PendingIntent deliveredPendingIntent = PendingIntent.getBroadcast(context, 0, smsDeliveredIntent, PendingIntent.FLAG_ONE_SHOT);

//         SmsManager sms = SmsManager.getDefault();
//         sms.sendTextMessage(smsPhone, null, "TEST", sentPendingIntent, deliveredPendingIntent);
//
//         if(true)
//            return;


            ArrayList<String> smsBodyParts = smsManager.divideMessage(smsMessage);
            ArrayList<PendingIntent> sentPendingIntents = new ArrayList<>();
            ArrayList<PendingIntent> deliveredPendingIntents = new ArrayList<>();

            for (int i = 0; i < smsBodyParts.size(); i++) {
                sentPendingIntents.add(sentPendingIntent);
                deliveredPendingIntents.add(deliveredPendingIntent);
            }

// Send a text based SMS
            smsManager.sendMultipartTextMessage(smsPhone, null, smsBodyParts, sentPendingIntents, deliveredPendingIntents);

        }
    }

    private void sendSMS(Context context, Handler h, String smsId, String smsPhone, String smsMessage) {
        new SMSSendThread(context, h, smsId, smsPhone, smsMessage).start();
    }


    private void updateSmsStatus(final Context context, final String smsId, final int smsStatusToUpdate) {

        updateSMSStatusIntoDB(context, smsId, smsStatusToUpdate);

        sendUpdateSMSStatusToServer(smsId, smsStatusToUpdate);
    }

    private void sendUpdateSMSStatusToServer(final String smsId, final int smsStatusToUpdate) {

        new Thread(new Runnable() {
            @Override
            public void run() {

                HttpURLConnection urlConnection = null;
                String urlStr = API.funcSetSmsStatus(smsId, "" + smsStatusToUpdate);
                URL url = null;

                try {
                    url = new URL(urlStr);

                    urlConnection = (HttpURLConnection) url.openConnection();

                    InputStream in = urlConnection.getInputStream();

                    StringBuilder sb = new StringBuilder();

                    int readBites = 0;
                    byte[] buffer = null;
                    while ((readBites = in.read(buffer = new byte[50])) != -1) {

                        if (readBites == buffer.length)
                            sb.append(new String(buffer));
                        else {
                            byte bufferLast[] = new byte[readBites];
                            System.arraycopy(buffer, 0, bufferLast, 0, bufferLast.length);
                            sb.append(new String(bufferLast));
                        }
                    }

                    final String response = sb.toString();

//                    log(response, true, true);

                    if (TextUtils.isEmpty(response)) {
//                        log("funcSetSmsStatus - Error: response EMPTY", true, true);
                        return;
                    }

                    try {
                        JSONObject jsonRoot = new JSONObject(response);

                        final String errorStr = jsonRoot.optString("error");
                        if (errorStr.length() > 0) {
//                            log("funcSetSmsStatus - Error: " + errorStr, true, true);
                        }

                        final int status = jsonRoot.optInt("status");
                        final String smsid = jsonRoot.optString("smsid");

                        updateSmsList(smsid, status);

                    } catch (final JSONException e) {
//                        log(e.getMessage(), true, true);
                    }

                } catch (MalformedURLException e1) {
//                    log(e1.getMessage(), true, true);
                } catch (IOException e) {
//                    log(e.getMessage(), true, true);
                } finally {
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                }

            }
        }).start();
    }


    private void updateSmsList(final String smsId, final int smsStatusToUpdate) {

        h.post(new Runnable() {
            @Override
            public void run() {

//            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
//            v.vibrate(100);

                if (MainActivity.activity != null)
                    MainActivity.activity.smsStatusChanged();
            }
        });
    }

    private void updateSMSStatusIntoDB(final Context context, final String smsId, final int smsStatusToUpdate) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        if (prefs.contains(Constants.KEY_SMS_JSON)) {
            try {
                JSONArray smsArrSrc = new JSONArray(prefs.getString(Constants.KEY_SMS_JSON, ""));
                JSONArray smsArrDst = new JSONArray();

                for (int i = 0; i < smsArrSrc.length(); i++) {
                    JSONObject smsJson = smsArrSrc.optJSONObject(i);

                    String smsIdSaved = smsJson.optString(SMS.KEY_SMS_ID);

                    if (smsIdSaved.equals(smsId))
                        smsJson.put(SMS.KEY_RESPONSE, smsStatusToUpdate);

                    smsArrDst.put(smsJson);
                }

                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(Constants.KEY_SMS_JSON, smsArrDst.toString());
                editor.commit();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    class SMSSendReceiver extends BroadcastReceiver {

        public static final String ACTION_SMS_SENT = "SMS_SENT";

        @Override
        public void onReceive(Context context, Intent intent) {

            String smsId = "";

            if (intent != null) {

                if (intent.hasExtra(Constants.KEY_INTENT_SMS_ID))
                    smsId = intent.getStringExtra(Constants.KEY_INTENT_SMS_ID);
                else
                    return;
            }

            StringBuilder sbLog = new StringBuilder("SEND id=" + smsId);

            int smsStatusToUpdate = SMS.SMS_STATUS_NOT_SEND;

            switch (getResultCode()) {

                case Activity.RESULT_OK:
                    sbLog.append(" : SEND!!!");

                    smsStatusToUpdate = SMS.SMS_STATUS_SEND;

                    break;

                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    sbLog.append("NO : Generic failure cause");
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    sbLog.append("NOT : Service is currently unavailable");
                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    sbLog.append("NOT : No pdu provided");
                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    sbLog.append("NOT : Radio was explicitly turned off");
                    break;
                case Activity.RESULT_CANCELED:
                    sbLog.append(" : Activity.RESULT_CANCELED!!!");
                    break;
                default:
                    sbLog.append(" : default");

                    break;
            }

            log(sbLog.toString(), false, true);

            updateSmsStatus(context, smsId, smsStatusToUpdate);

        }

    }

    class SMSDeliveredReceiver extends BroadcastReceiver {

        public static final String ACTION_SMS_DELIVERED = "SMS_DELIVERED";

        @Override
        public void onReceive(Context context, Intent intent) {

            log("DELIVER BEFORE getId" + getResultCode(), false, true);

            String smsId = "";

            if (intent != null) {
                if (intent.hasExtra(Constants.KEY_INTENT_SMS_ID))
                    smsId = intent.getStringExtra(Constants.KEY_INTENT_SMS_ID);
                else {
                    log("DELIVER NO SMSID", false, true);
                    return;
                }
            }

            int smsStatusToUpdate = 0;

            log("BEFORE setStatus code = " + getResultCode(), false, true);

            switch (getResultCode()) {

                case Activity.RESULT_OK:
                    log("DELIVER id=" + smsId + " : DELIVERED<<", false, true);
                    smsStatusToUpdate = SMS.SMS_STATUS_DELIVERED;
                    break;

                case Activity.RESULT_CANCELED:
                    log("DELIVER id=" + smsId  + " : SMS not delivered", false, true);
                    smsStatusToUpdate = SMS.SMS_STATUS_NOT_DELIVERED;
                    break;

                default:
                    log("DELIVER id=" + smsId  + " : DEFAULT : " + getResultCode(), false, true);

                    break;
            }

            updateSmsStatus(context, smsId, smsStatusToUpdate);

        }
    }

    public void vibrate() {
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        long[] pattern = {0, 100, 500, 100};
        v.vibrate(pattern, -1);
    }

    public void log(final String message, boolean showToast, boolean consoleLog) {

        if (showToast) {
            h.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(ProcessSMSService.this, message, Toast.LENGTH_SHORT).show();
                }
            });
        }

        if (consoleLog)
            Log.d(ProcessSMSService.class.getSimpleName(), message);
    }
}