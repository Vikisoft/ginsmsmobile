package ua.kiev.vikisoft.vikisoftsmssender;

import java.util.Date;

/**
 * Created by Pavel on 05.03.2016.
 */
public class SMS {

    public static final String KEY_SMS_ID = "sms_id";
    public static final String KEY_PHONE = "phone";
    public static final String KEY_RESPONSE = "response";
    public static final String KEY_MESSAGE = "message";
    public static final String KEY_LIVETIME = "livetime";
    public static final String KEY_SHOW_DATE = "KEY_SHOW_DATE";
    public static final String KEY_IS_NEW = "KEY_IS_NEW";

    public static final int SMS_STATUS_WAIT = 100;
    public static final int SMS_STATUS_SEND = 200;
    public static final int SMS_STATUS_DELIVERED = 300;
    public static final int SMS_STATUS_NOT_SEND = 400;
    public static final int SMS_STATUS_NOT_DELIVERED = 500;

    public String smsId;
    public String smsPhone;
    public int smsResponse;
    public String smsMessage;
    public String livetime;

    public String showDate;
    public boolean isNew;

    public SMS(String smsId, String smsPhone, int smsResponse, String smsMessage, String livetime, String showDate, boolean isNew) {
        this.smsId = smsId;
        this.smsPhone = smsPhone;
        this.smsResponse = smsResponse;
        this.smsMessage = smsMessage;
        this.livetime = livetime;
        this.showDate = showDate;
        this.isNew = isNew;
    }
}
