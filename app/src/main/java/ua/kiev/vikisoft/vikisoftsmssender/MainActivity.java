package ua.kiev.vikisoft.vikisoftsmssender;

import android.Manifest;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static ua.kiev.vikisoft.vikisoftsmssender.Constants.LAST_SAVED_SMS_AMOUNT;

public class MainActivity extends AppCompatActivity {

    public static MainActivity activity;

    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 29;  // unique request code


    ArrayList<SMS> smsList;
    ListView lvSMSList;

    ServiceConnection sConn;
    boolean bound = false;
    private Intent serviceIntent;
    private SMSAdapter ad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.activity = this;

        boolean appAbleToSendSMS = checkSEND_SMSPermission();

        if(appAbleToSendSMS)
            init();
        else
            showSMSSendingProblemDialog();

    }


    private boolean checkSEND_SMSPermission() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.SEND_SMS)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.SEND_SMS)) {

                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.SEND_SMS},
                            MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }

                return false;
            }
            else
                return true;
        }
        else
            return true;
    }

    private void init() {

        lvSMSList = (ListView) findViewById(R.id.lvSMSList);
        smsList = new ArrayList<>();
        smsList = reloadList();
        ad = new SMSAdapter(this, R.layout.sms_list_item, smsList);
        lvSMSList.setAdapter(ad);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if(!prefs.contains(Constants.KEY_SERVER_URL)) {
            FragmentManager fm = getFragmentManager();
            ServerConnectionDialog sdf = new ServerConnectionDialog();
            sdf.show(fm, "dialog");
        }

//        >>>>TEST
//        SharedPreferences.Editor editor = prefs.edit();
//        editor.remove(Constants.KEY_SMS_JSON);
//        editor.commit();


        serviceIntent = new Intent(getBaseContext(), ProcessSMSService.class);

        sConn = new ServiceConnection() {
            public void onServiceConnected(ComponentName name, IBinder binder) {
                Toast.makeText(MainActivity.this, "onServiceConnected!!!!", Toast.LENGTH_LONG).show();
                bound = true;
            }

            public void onServiceDisconnected(ComponentName name) {
                Toast.makeText(MainActivity.this, "onServiceDisconnected!!!!", Toast.LENGTH_LONG).show();
                bound = false;
            }
        };
    }

    private void showSMSSendingProblemDialog() {
        FragmentManager fm = getFragmentManager();
        DeviceNotAbleToSendSMSDialog dnatsd = new DeviceNotAbleToSendSMSDialog();
        dnatsd.show(fm, "dialog");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    init();

                } else {

                    showSMSSendingProblemDialog();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem mi = menu.findItem(R.id.action_service_start);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        mi.setEnabled(prefs.contains(Constants.KEY_SERVER_URL));

        if(ProcessSMSService.isServiceRun){
            mi.setTitle(R.string.action_service_stop);
            mi.setChecked(true);
        }
        else{
            mi.setTitle(R.string.action_service_start);
            mi.setChecked(false);
        }


        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case R.id.change_server_url:
                FragmentManager fm = getFragmentManager();
                ServerConnectionDialog sdf = new ServerConnectionDialog();
                sdf.show(fm, "dialog");
                break;

            case R.id.action_service_start:


                if(item.isChecked()){
                    stopService(serviceIntent);
                }
                else{
                    startService(serviceIntent);
                }


                break;

//            case R.id.action_service_bind:
//
//                if(item.isChecked()) {
//                    bindService(serviceIntent, sConn, BIND_AUTO_CREATE);
//                }
//                else {
//                    if (!bound)
//                        return true;
//                    unbindService(sConn);
//                    bound = false;
//                }
//
//                break;

//            case R.id.send_sms:
//
//                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
//                SharedPreferences.Editor editor = prefs.edit();
//                editor.remove(Constants.KEY_SMS_JSON);
//                editor.commit();
//
//                ad.clearList();
//
//
//                break;

        }

        item.setChecked(!item.isChecked());

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    protected void onDestroy() {
        super.onDestroy();

//        if (!bound)
//            return;
//        unbindService(sConn);
//        bound = false;

    }

    public void smsStatusChanged() {
        ad.smsStatusChanged();
    }

    private ArrayList<SMS> reloadList() {
        smsList.clear();
        return getSmsListFromJSON();
    }

    private ArrayList<SMS> getSmsListFromJSON() {

        ArrayList<SMS> smsList = new ArrayList<>();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if(prefs.contains(Constants.KEY_SMS_JSON)) {
            try {
                JSONArray smsArr = new JSONArray(prefs.getString(Constants.KEY_SMS_JSON, ""));

                for(int i = 0; i < smsArr.length(); i++){
                    JSONObject smsJson = smsArr.optJSONObject(i);

                    String smsSmsId    = smsJson.optString(SMS.KEY_SMS_ID);
                    String smsPhone    = smsJson.optString(SMS.KEY_PHONE);
                    int smsResponse    = smsJson.optInt(SMS.KEY_RESPONSE);
                    String smsMessage  = smsJson.optString(SMS.KEY_MESSAGE);
                    String smsLivetime = smsJson.optString(SMS.KEY_LIVETIME);
                    String showDate    = smsJson.optString(SMS.KEY_SHOW_DATE);
                    boolean isNew      = smsJson.optBoolean(SMS.KEY_IS_NEW);

                    SMS sms = new SMS(smsSmsId, smsPhone, smsResponse, smsMessage, smsLivetime, showDate, isNew);

                    smsList.add(sms);

                }

                if(ad != null)
                    ad.notifyDataSetChanged();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return smsList;
    }

    class SMSAdapter extends ArrayAdapter<SMS>{

        List<SMS> smsList;
        int itemLayoutRes;

        public SMSAdapter(Context context, int resource, List<SMS> objects) {
            super(context, resource, objects);
            itemLayoutRes = resource;
            smsList = objects;
        }

        @Override
        public int getCount() {
            return smsList.size();
        }

        @Override
        public SMS getItem(int position) {
            return smsList.get(position);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if(convertView == null){
                convertView = LayoutInflater.from(getContext()).inflate(itemLayoutRes, parent, false);
            }

            convertView.setBackgroundColor(Color.TRANSPARENT);

            TextView tvSmsId = (TextView) convertView.findViewById(R.id.tvSmsId);
            tvSmsId.setText(smsList.get(position).smsId);

            TextView tvSmsPhone = (TextView) convertView.findViewById(R.id.tvSmsPhone);
            tvSmsPhone.setText(smsList.get(position).smsPhone);

            TextView tvSmsDate = (TextView) convertView.findViewById(R.id.tvSmsDate);
            tvSmsDate.setText(smsList.get(position).showDate);

            TextView tvSmsMessage = (TextView) convertView.findViewById(R.id.tvSmsMessage);
            tvSmsMessage.setText(smsList.get(position).smsMessage);

            ImageView ivSmsStatus = (ImageView) convertView.findViewById(R.id.ivSmsStatus);
            switch (smsList.get(position).smsResponse){
                case SMS.SMS_STATUS_WAIT:
                    ivSmsStatus.setImageResource(R.drawable.sms_wait);
                    break;
                case SMS.SMS_STATUS_SEND:
                    ivSmsStatus.setImageResource(R.drawable.sms_sent);
                    break;
                case SMS.SMS_STATUS_DELIVERED:
                    ivSmsStatus.setImageResource(R.drawable.sms_delivered);
                    break;
                case SMS.SMS_STATUS_NOT_SEND:
                    ivSmsStatus.setImageResource(R.drawable.sms_not_sent);
                    break;
                case SMS.SMS_STATUS_NOT_DELIVERED:
                    ivSmsStatus.setImageResource(R.drawable.sms_not_delivered);
                    break;
            }

            if(smsList.get(position).isNew)
                convertView.setBackgroundColor(Color.parseColor("#ffdede"));

            return convertView;
        }

        public void clearList() {
            smsList.clear();

            notifyDataSetChanged();
        }

        public void smsStatusChanged() {
            smsList = reloadList();
        }
    }

    public static class DeviceNotAbleToSendSMSDialog extends DialogFragment implements DialogInterface.OnClickListener {

        final String LOG_TAG = "myLogs";

        public DeviceNotAbleToSendSMSDialog() {
        }

        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.send_sms_permission_problem_mess)
                    .setPositiveButton(android.R.string.ok, this);
            return adb.create();
        }

        public void onClick(DialogInterface dialog, int which) {
            getActivity().finish();
        }
    }
}